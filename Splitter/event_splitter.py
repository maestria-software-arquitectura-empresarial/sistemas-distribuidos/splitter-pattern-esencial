import json
import requests

# Componente que recibe y divide los eventos en subeventos
class EventSplitter:
    def __init__(self, api_url):
        self.api_url = api_url

    def process_order_event(self, event):
        # Dividir el evento en subeventos basados en los productos pedidos
        subevents = event["products"]
        for subevent in subevents:
            self.send_subevent(subevent)

    def send_subevent(self, subevent):
        # Enviar el subevento al microservicio correspondiente a través de la API
        try:
            response = requests.post(self.api_url, json=subevent)
            if response.status_code == 200:
                print(f"Subevento '{subevent['name']}' enviado corretamente a microservicio.")
            else:
                print(f"Fallo envio al Subevento '{subevent['name']}' el microservicio. Status code: {response.status_code}")
        except requests.exceptions.RequestException as e:
            print(f"Error ocurrio mientras se envia Subevente '{subevent['name']}' to microservice: {e}")