# Microservicios que manejan los subeventos
class PaymentService:
    def process_payment(self, subevent):
        # Simular el procesamiento de pagos
        print(f"Procesando pago para el producto '{subevent['name']}'")