from order_event_generator import OrderEventGenerator
from event_splitter import EventSplitter
from payment_service import PaymentService
from inventory_service import InventoryService
import requests

# Cliente
if __name__ == "__main__":
    # URL de la API del Divisor de Eventos
    event_splitter_api_url = "http://localhost:5000/api/process_event"

    # URL de las APIs de los microservicios
    payment_service_api_url = "http://localhost:5000/api/process_payment"
    inventory_service_api_url = "http://localhost:5000/api/update_inventory"

    # Crear instancias de los componentes
    order_event_generator = OrderEventGenerator()
    event_splitter = EventSplitter(event_splitter_api_url)
    payment_service = PaymentService()
    inventory_service = InventoryService()

    # Generar un evento de pedido
    order_event = order_event_generator.generate_order_event()

    # Enviar el evento al Divisor de Eventos
    event_splitter.process_order_event(order_event)