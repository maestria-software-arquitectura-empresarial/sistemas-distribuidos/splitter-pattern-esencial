import random

# Componente que genera eventos de pedidos
class OrderEventGenerator:
    def generate_order_event(self):
        customer_id = random.randint(1000, 9999)
        products = self.generate_random_products()
        return {"customer_id": customer_id, "products": products}

    def generate_random_products(self):
        products = ["Shirt", "Pants", "Shoes", "Hat"]
        num_products = random.randint(1, 4)
        return [{"name": random.choice(products), "quantity": random.randint(1, 3)} for _ in range(num_products)]
